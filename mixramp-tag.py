#!/usr/bin/python
#
# A script to add mixramp tags for audio files that support metadata.
# Currently supports FLAC, MP3 and M4A.

import argparse
import os
import subprocess
import tempfile

_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
_ANALYZER = os.path.join(_SCRIPT_DIR, 'mixramp')

_TAG_START = 'MIXRAMP_START'
_TAG_END = 'MIXRAMP_END'
_TAG_REF = 'MIXRAMP_REF'


def _process_wav_file(path, output_path, args):
  lines = subprocess.check_output([_ANALYZER, path])
  if args.verbose:
    print 'Analisys output:\n%s' % lines
  start = None
  end = None
  ref = None
  for line in lines.split('\n'):
    if not line:
      continue
    if line.startswith(_TAG_START + '='):
      start = line[len(_TAG_START) + 1:]
    elif line.startswith(_TAG_END + '='):
      end = line[len(_TAG_END) + 1:]
    elif line.startswith(_TAG_REF + '='):
      ref = line[len(_TAG_REF) + 1:]
    else:
      raise Exception('Unsupported mixramp output: %s' % lines)
  if not args.quiet:
    print '%s=%s' % (_TAG_REF, ref)
    print '%s=%s' % (_TAG_START, start)
    print '%s=%s' % (_TAG_END, end)
  if args.write:
    _write_tags(start, end, ref, output_path, args)


def _process_file(path, args):
  tags = subprocess.check_output(['mid3v2', '-l', path])
  for tag in tags.split('\n'):
    if tag.startswith('TXXX=mixramp_start='):
      if not args.quiet:
        print 'File already has mixramp info: %s' % path
      return
  if path.endswith('.wav'):
    if args.verbose:
      print 'Converting %s' % path
    _process_wav_file(path, path, args)
    return
  with tempfile.NamedTemporaryFile(suffix='.wav') as f:
    conv_args = ['avconv', '-y', '-i', path, '-ar', '96k']
    if args.verbose:
      print 'Converting %s to WAV format' % path
    else:
      conv_args += ['-loglevel', 'error']
    subprocess.check_call(conv_args + [f.name])
    _process_wav_file(f.name, path, args)


def _write_tags(start, end, ref, output_path, args):
  if output_path.endswith('.mp3') or output_path.endswith('.m4a'):
    #command = ['eyeD3', '-2', '--strict', '--set-encoding=utf8',
    #           '--set-user-text-frame=mixramp_start:%s' % start,
    #           '--set-user-text-frame=mixramp_end:%s' % end,
    #           '--set-user-text-frame=mixramp_ref:%s' % ref,
    #           output_path]
    command = ['mid3v2',
               '--TXXX', 'mixramp_start:%s' % start,
               '--TXXX', 'mixramp_end:%s' % end,
               '--TXXX', 'mixramp_ref:%s' % ref,
               output_path]
  elif output_path.endswith('.flac'):
    command = ['metaflac', '--remove-tag=MIXRAMP_START',
               '--set-tag=MIXRAMP_START=%s' % start,
               '--remove-tag=MIXRAMP_END', '--set-tag=MIXRAMP_END=%s' % end,
               '--remove-tag=MIXRAMP_REF', '--set-tag=MIXRAMP_REF=%s' % end,
               output_path]
  else:
    raise Exception('Cannot write tags to %s', output_path)

  if args.verbose:
    print 'Writing tags as %s' % command

  subprocess.check_call(command)


def main():
  parser = argparse.ArgumentParser(description='Calculate mixramp tags')
  parser.add_argument('-w', dest='write', action='store_true',
                      help='Write tags to the file (off by default)')
  parser.add_argument('-q', dest='quiet', action='store_true',
                      help='Do not print tag values to stdout')
  parser.add_argument('-v', dest='verbose', action='store_true',
                      help='Print verbose output')
  parser.add_argument('files', nargs='*', help='File names')
  args = parser.parse_args()

  for path in args.files:
    _process_file(path, args)


main()

